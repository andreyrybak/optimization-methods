{-# OPTIONS_GHC
-XExistentialQuantification
-XBangPatterns
#-}
import Text.Printf
import qualified Data.List as L

import Fusion

type FPType = Double
type Domain = (FPType, FPType)
type Codomain = FPType
type FunctionType = Domain -> Codomain
type FPair = (Domain, Codomain)
f :: FunctionType
f (x, y) = exp (- x ** 2 - y ** 2) * (2 * x ** 2 + 3 * y ** 2)

f'x, f'y, f''xx, f''yy, f''xy :: FunctionType
f'x (x, y) = - 2 * x * exp (- x ** 2 - y ** 2) * (2 * x ** 2 + 3 * y ** 2 - 2)
f'y (x, y) = - 2 * y * exp (- x ** 2 - y ** 2) * (2 * x ** 2 + 3 * y ** 2 - 3)
f''xx (x, y) = exp (- x ** 2 - y ** 2) * (8 * x ** 4 + 4 * x ** 2 * (3 * y ** 2 - 5) - 6 * y ** 2 + 4)
f''yy (x, y) = exp (- x ** 2 - y ** 2) * (x ** 2 * (8 * y ** 2 - 4) + 6 * (2 * y ** 4 - 5 * y ** 2 + 1))
f''xy (x, y) = 4 * x * y * exp (- x ** 2 - y ** 2) * (2 * x ** 2 + 3 * y ** 2 - 5)

pairwiseList :: [a] -> [PairS a a]
pairwiseList xs = zipWithList (makePair) xs $! tailList xs

newtonList :: FunctionType -> FunctionType -> FunctionType -> FunctionType -> FunctionType -> FunctionType -> FPType -> Domain -> [FPair]
newtonList g g'x g'y g''xx g''xy g''yy eps =
  mapList (\x -> (x, g x)) . mapList fst'
  . takeWhile1List ((> eps) . norm . uncurry' diff)
  . pairwiseList . (iterateList next)
    where
    norm :: Domain -> FPType
    norm (x, y) = sqrt $ x ** 2 + y ** 2
    diff :: Domain -> Domain -> Domain
    diff (x1, y1) (x2, y2) = (abs $ x1 - x2, abs $ y1 - y2)
    next :: Domain -> Domain
    next u@(x, y) = (x - dx, y - dy)
      where
      dx = (h22 * fx - h12 * fy) / det
      dy = (h11 * fy - h12 * fx) / det
      fx = g'x u ; fy = g'y u
      h11 = g''xx u
      h22 = g''yy u
      h12 = g''xy u
      det = det22 h11 h12 h12 h22
      det22 :: Codomain -> Codomain -> Codomain -> Codomain -> Codomain
      det22 a11 a12 a21 a22 = a11 * a22 - a12 * a21

u0 :: Domain
u0 = (0.2, 0.1)
epsilonPowers :: [Integer]
epsilonPowers = [-1, -3, -5]
calcEpsilon :: Integer -> FPType
calcEpsilon n = 10.0 ** fromInteger n
epsilons :: [FPType]
epsilons = map calcEpsilon epsilonPowers
     
showFPType :: FPType -> String
showFPType = printf "%.8f"
texTab = " & "
myShow :: FPair -> String
myShow ((x,y),f) = concat $ L.intersperse texTab $ map showFPType [x, y, f]

printOne :: FPType -> [FPair] -> String
printOne eps as =
  (concat $
    (L.intersperse "\\\\\n" $
      map (\ (i,s) -> show i ++ texTab ++ s) $
        zip [1..] (map myShow as)
    )
  ) ++ "\n\n"

main :: IO ()
main = do
  mapM_ (\eps -> writeFile (showFPType eps ++ ".out") $ printOne eps $ newtonList f f'x f'y f''xx f''xy f''yy eps u0) epsilons
